---
title: 'API'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 3
summary: Usage of the API from scripts and other services
---
### Usage

You can post your data as a csv file attachment with either url parameters or form fields to the following url:

https://wheatpredictor.azurewebsites.net/api/predict

and it will respond with json data of your results.

make sure you include the 'model' parameter for 

example in curl: 

```bash
curl 'https://wheatpredictor.azurewebsites.net/api/predict' \
    -F 'model=cnn_single' \
    -F 'jumps=1000,1800' \
    -F 'reflectance=@example.csv'
```


### Parameters

This is a list of parameters you can provide the API.

- *model*  - Which model to use, ust be one of the following:
    - cnn_single
    - cnn_multi
    - pls
    - ensemble
- *jumps* - Break points of the sensors, must be a comma separated list
- *reflectance* - your csv file