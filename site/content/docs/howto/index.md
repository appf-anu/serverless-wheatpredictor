---
title: 'How to use this predictor?'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 1
summary: How to use this predictor.
---

1. The csv file must have a maximum of 200 observations and be under 21Kb. If it is necessary, use multiple files because the website will present an error if the file uploaded is too large. The header requirements are that **Wavelengths (350-2500 nm) should be in the first column** and other columns should represent a measurement with the leaf-clip.

*Example:*

![Example File](/images/example-file.png)

2. Check the break points (jumps) of the sensors in the spectra, usually the first one is at 1000 nm, the second one often changes to 1780nm, 1800nm or 1830nm. You can observe the jump of the spectra using the program ViewSpecPro, SAMS, excel or any program that allow you to plot the whole spectra.

3. Select 'Browse...' to choose your csv file to process, this should open a graph to view your input data.

4. Use the dropdown menu to select the model to predict your data, the detailed descriptions of these models can be viewed in .

5. Click 'Submit', your browser should start a file download with your results once it is ready.