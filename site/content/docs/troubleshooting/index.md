---
title: Troubleshooting & Recommendations
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 3
summary: Some common problems and recommendations
---


### Troubleshooting / FAQ

> The trigger is loose

Screw it again.

---
> I see half of the spectra but not all the spectra. 

For some reason one detector did not turn on. Turn off laptop and ASD. Turn on first ASD then laptop.

---
> I do not see the spectra. 

Turn on the light of the leaf-clip, if it is on. Turn off laptop and ASD. Turn on first ASD then laptop.

---
> 'Unable to Collect Swir Timeout Error -211. Failed during sample count command. Header=200, ErrByte=-11,Command=C,1,30,0' 

Machine is not receiving enough power to run. Check if cable that connects to the power or battery is working correctly

---
> How can I know that ASD Fieldspec is well calibrated? 

Measure with the white background with 'Mylar' and send the reading to ASD experts, eg. David Scattergood dscattergood@portableas.com.au.

---
> The mask was placed in wrong position. 

Repeat measurements with the good position of the mask.

---
> The measurements were done with a white background. 

There is nothing wrong with this measurements but if you want to use the 'Wheat Physiology Predictor' you need to repeat measurements with the black background.

---
> WiFi of ASD is not connecting with laptop. R=Re-start ASD and laptop, this time turn on ASD first and then the laptop. If this does not work, use the Ethernet cable.

---
> I have more questions. 

Check the manuals for the ASD:

- [RS3 User Manual](http://www2.fct.unesp.br/docentes/carto/enner/PPGCC/Hiperespectral/Espectrorradiometro%20-%20Manuais%20-%20Campo/RS3%20User%20Guide.pdf)
- [ViewSpec Pro](http://geoinfo.amu.edu.pl/geoinf/m/spektr/viewspecpro.pdf)



### Recommendations

- Measure SPAD with SPAD Minolta in the same place were reflectance was measured. This data can be compared with SPAD predicted from reflectance. Usually both traits correlate positively, the R2 can be low, but the correlation is positive. Comparing SPAD from a SPAD Minolta with SPAD predicted from ASD is a good check and a good way to find outliers.

- If you are good with computers, you can record the number of spectrum in 'Notepad', the plot/pot number and even the SPAD value.

- Do not touch the panel of white or black background to avoid damage. Calibration is better if they are clean and intact.

- Do not bend the fibre-optic cable, it is fragile and very expensive, if it is damaged, measurements cannot be done and it will need to be fixed.

- Be careful with the little screw of the leaf-clip, do not drop it or lose it as it is needed to hold the mask.

- Double check that the mask is placed in the correct position, otherwise you will need to repeat the measurements.

- Close carefully the leaf-clip and ensure that it closes in the centre, do not measure if it closes towards a side or if it does not close well.

- In the field have one or more spare batteries for the laptop and for the ASD. Do not forget to charge the batteries before you go into the field.

- Follow the protocol, it is important to first turn on the ASD and then the laptop, otherwise part of the detectors might turn on wrong.

- For measurements in the field, start working as soon as the ASD is on because it will be using precious battery life. If you take a break and the machine is on, there is a risk is that the battery will drain before you finish the measurements.

- As most of the machines, avoid to damage the ASD. Avoid getting it wet, etc. During long trips keep it in the big black box, well protected. It is very expensive equipment.

- When saving the files, give different names to the file each time to avoid them being overwritten and losing valuable information.