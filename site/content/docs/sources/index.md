---
title: 'Sources & Acknowledgements'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 6
summary: Sources, citations, acknowledgements and Git repositories.
---

### Research and Machine Learning Models

**Please cite:**

Furbank, R.T., Silva-Perez, V., Evans, J.R. et al. Wheat physiology predictor: predicting physiological traits in wheat from hyperspectral reflectance measurements using deep learning. Plant Methods 17, 108 (2021). https://doi.org/10.1186/s13007-021-00806-6

##### Australian National Univeristy

- [Prof Robert Furbank](https://biology.anu.edu.au/people/academics/robert-furbank)

- [Wennan He](https://cecs.anu.edu.au/people/wennan-he)

##### LaTrobe University

- [Zhen He](http://homepage.cs.latrobe.edu.au/zhe/)

- [Ashley Hall](https://scholars.latrobe.edu.au/awhall/publications)


### This Site & the Azure Backend

Staff from Australian Plant Phenomics Facility, ANU:

- Gareth Dunstone gareth.dunstone@anu.edu.au
- Richard Poire richard.poire@anu.edu.au

[Gitlab repo for this site](https://gitlab.com/appf-anu/serverless-wheatpredictor)

[Original models](https://github.com/ashwhall/hyperspec-trait-prediction)
