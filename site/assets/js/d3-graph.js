'use strict'
// d3 must exist
/* global d3 */
// set the dimensions and margins of the graph
const margin = {
  top: 10,
  right: 0,
  bottom: 50,
  left: 50
}

const width = document.getElementById('d3-graph-area').offsetWidth - margin.left - margin.right
const height = 400 - margin.top - margin.bottom
const bisectr = d3.bisector((d) => d.Wavelength).left

// append the svg object to the body of the page
const svg = d3.select('#d3-graph-area')
  .append('svg')
  .attr('width', width + margin.left + margin.right)
  .attr('height', height + margin.top + margin.bottom)
  .append('g')
  .attr('transform',
    'translate(' + margin.left + ',' + margin.top + ')')

let line, x, xAxis, y, yAxis, focus, selectedOption, data

// When the button is changed, run the updateChart function
d3.select('#d3-graph-column-select').on('change', function (d) {
  // recover the option that has been chosen
  selectedOption = d3.select(this).property('value')
  update(selectedOption)
})

function update (selectedOption) {
  const dataFilter = data.map((d) => {
    return {
      Wavelength: d.Wavelength,
      value: d[selectedOption]
    }
  })

  y = d3.scaleLinear()
    .domain(d3.extent(dataFilter, d => +d.value))
    .range([height, 0])
  yAxis.call(d3.axisLeft(y))

  x = d3.scaleLinear()
    .domain(d3.extent(dataFilter, d => +d.Wavelength))
    .range([0, width])
  xAxis.call(d3.axisBottom(x))

  // Give these new data to update line
  line
    .datum(dataFilter)
    .transition()
    .duration(1000)
    .attr('d', d3.line()
      .x((d) => x(+d.Wavelength))
      .y((d) => y(+d.value))
    )
}

function mousemove () {
  const x0 = x.invert(d3.mouse(this)[0])
  const i = bisectr(data, x0, 1)
  const d0 = data[i - 1]
  const d1 = data[i]
  const d = x0 - +d0.Wavelength > +d1.Wavelength - x0 ? d1 : d0
  focus.attr('transform', `translate(${x(+d.Wavelength)}, ${y(+d[selectedOption])})`)
  focus.select('line.x')
    .attr('x1', 0)
    .attr('x2', -x(d.Wavelength))
    .attr('y1', 0)
    .attr('y2', 0)

  focus.select('line.y')
    .attr('x1', 0)
    .attr('x2', 0)
    .attr('y1', 0)
    .attr('y2', height - y(+d[selectedOption]))

  focus.select('text').text(`${+d.Wavelength}nm`)
}

function redrawGraph (event) {
  const file = event.target.files[0]
  const reader = new FileReader()
  reader.addEventListener('load', () => {
    data = d3.csvParse(reader.result)
    const zeroColumn = data.columns[0]
    data.forEach((d) => {
      d.Wavelength = d[zeroColumn]
    })
    // clear the graph and selector
    svg.selectAll('*').remove()
    d3.select('#d3-graph-column-select').selectAll('*').remove()
    d3.select('#d3-graph-column-select').style('display', null)
    const fields = Object.keys(data[0]).filter((a) => a !== 'Wavelength' && a !== zeroColumn)
    d3.select('#d3-graph-column-select')
      .selectAll('myOptions')
      .data(fields)
      .enter()
      .append('option')
      .text(d => d) // text showed in the menu
      .attr('value', d => d) // corresponding value returned by the button

    selectedOption = d3.select('#d3-graph-column-select').property('value')

    const dataFilter = data.map((d) => {
      return {
        Wavelength: d.Wavelength,
        value: d[selectedOption]
      }
    })
    x = d3.scaleLinear()
      .domain(d3.extent(dataFilter, d => +d.Wavelength))
      .range([0, width])
    xAxis = svg.append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x))
      // Add Y axis
    y = d3.scaleLinear()
      .domain(d3.extent(dataFilter, d => +d.value))
      .range([height, 0])
    yAxis = svg.append('g')
      .call(d3.axisLeft(y))
    line = svg
      .append('g')
      .append('path')
      .datum(dataFilter)
      .style('stroke-width', 2)
      .style('stroke', '#658d1b')
      .style('fill', 'none')

    svg.append('path')
      .datum(data)
      .attr('class', 'line')
      .attr('d', line)

    focus = svg.append('g')
      .attr('class', 'focus')
      .style('display', 'none')

    focus.append('circle')
      .attr('r', 4.5)

    focus.append('line')
      .classed('x', true)

    focus.append('line')
      .classed('y', true)

    focus.append('text')
      .attr('x', 9)
      .attr('dy', '.35em')

    svg.append('rect')
      .attr('class', 'overlay')
      .attr('width', width)
      .attr('height', height)
      .on('mouseover', () => focus.style('display', null))
      .on('mouseout', () => focus.style('display', 'none'))
      .on('mousemove', mousemove)
    d3.select('.overlay')
      .style('fill', 'none')
      .style('pointer-events', 'all')

    d3.selectAll('.focus')
      .style('opacity', 0.7)

    d3.selectAll('.focus circle')
      .style('fill', 'none')
      .style('stroke', 'black')

    d3.selectAll('.focus line')
      .style('fill', 'none')
      .style('stroke', 'black')
      .style('stroke-width', '1.5px')
      .style('stroke-dasharray', '3 3')

    d3.select('#d3-graph-area').style('height', 'auto')
    update(selectedOption)
  })

  if (file) {
    reader.readAsText(file)
  }
}

document.getElementById('reflectance-file-field').addEventListener('change', redrawGraph)

// use click handler so that people dont have to change the value in order to get the
// correct device in the jumps field.
document.getElementById('jumps-device-select-field').addEventListener('click', (e) => {
  document.getElementById('jumps-text-field').value = e.target.value
})
