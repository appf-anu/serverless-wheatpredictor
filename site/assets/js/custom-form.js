'use strict'
const formElem = document.getElementById('myForm')
const alertTemplate = document.getElementById('alertTemplate')
const infoArea = document.getElementById('infoArea')
const btnElem = document.getElementById('submit-btn')
const spinnnerElem = document.getElementById('submit-btn')

function handleErrors (response) {
  if (response.ok) return response

  response.json().then((responseData) => {
    if (response.status === 400) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-warning')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
    if (response.status === 422) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-warning')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
    if (response.status === 500) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-danger')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
  }).catch((e) => {
    const alert = alertTemplate.content.firstElementChild.cloneNode(true)
    alert.classList.add('alert-danger')
    alert.getElementsByClassName('textarea')[0].textContent = response.statusText
    infoArea.appendChild(alert)
    setTimeout(() => {
      btnElem.classList.remove('disabled')
      spinnnerElem.classList.remove('spinner-grow')
      spinnnerElem.value = 'Submit'
    }, 2000)
  })
  return false
}

if (formElem) {
  formElem.onsubmit = async (e) => {
    const btnElem = document.getElementById('submit-btn')
    const spinnnerElem = document.getElementById('submit-btn')
    spinnnerElem.classList.add('spinner-grow')
    spinnnerElem.value = ''
    btnElem.classList.add('disabled')
    e.preventDefault()

    const formData = new FormData(formElem)
    fetch(formElem.action, {
      method: 'POST',
      body: formData
    })
      .then(handleErrors)
      .then((response) => {
        if (!response) return
        response.json().then((responseData) => {
          const fields = Object.keys(responseData[0])
          const replacer = (key, value) => {
            return value === null ? '' : value
          }
          let csv = responseData.map((row) => {
            return fields.map((fieldName) => {
              return JSON.stringify(row[fieldName], replacer)
            }).join(',')
          })
          csv.unshift(fields.join(','))
          csv = csv.join('\r\n')
          const dataFileName = `predicted-${formData.get('reflectance').name}`
          const blob = new Blob(['\ufeff', csv])

          const anchor = document.createElement('a')
          anchor.download = dataFileName
          anchor.href = (window.webkitURL || window.URL).createObjectURL(blob)
          anchor.dataset.downloadurl = ['application/csv', anchor.download, anchor.href].join(':')
          anchor.click()

          setTimeout(() => {
            btnElem.classList.remove('disabled')
            spinnnerElem.classList.remove('spinner-grow')
            spinnnerElem.value = 'Submit'
          }, 2000)
        })
      })
      .catch((e) => {
        const alert = alertTemplate.content.firstElementChild.cloneNode(true)
        alert.classList.add('alert-danger')
        alert.getElementsByClassName('textarea')[0].textContent = e
        infoArea.appendChild(alert)
        setTimeout(() => {
          btnElem.classList.remove('disabled')
          spinnnerElem.classList.remove('spinner-grow')
          spinnnerElem.value = 'Submit'
        }, 2000)
      })
  }
}
