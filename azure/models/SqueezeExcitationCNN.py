import math

import torch.nn as nn

from models.MostlyConvCNN import MostlyConvCNN


SQUEEZE_RATIO = 8


class GlobalAvgPool1D(nn.Module):
    def forward(self, x):
        return x.mean(-1)


class AddDim(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self._dim = dim

    def forward(self, x):
        return x.unsqueeze(self._dim)


class SEBlock(nn.Module):
    def __init__(self, inchannels, outchannels, kernel_size=3, stride=1, dilation=1, padding='VALID', last=False, squeeze_ratio=SQUEEZE_RATIO):
        super().__init__()
        squeeze_channels = outchannels // squeeze_ratio
        self.squeeze_excite = nn.Sequential(
            GlobalAvgPool1D(),
            nn.Linear(outchannels, squeeze_channels),
            nn.ReLU(inplace=True),
            nn.Linear(squeeze_channels, outchannels),
            nn.Sigmoid(),
            AddDim(-1)
        )
        layers = [nn.Conv1d(inchannels,
                            outchannels,
                            kernel_size=kernel_size,
                            stride=stride,
                            padding=self.padding_val(padding, stride, dilation, kernel_size),
                            dilation=dilation)]
        if not last:
            layers.extend([nn.BatchNorm1d(outchannels,
                                          eps=1e-05,
                                          momentum=0.1,
                                          affine=True,
                                          track_running_stats=True),
                           nn.ReLU(inplace=True)])

        self.seq = nn.Sequential(*layers)

    def forward(self, x):
        x = self.seq(x)
        return self.squeeze_excite(x) * x

    @staticmethod
    def padding_val(policy, stride, dilation, kernel_size):
        """Returns padding size for policies of 'VALID' and 'SAME'"""
        return 0 \
            if policy == 'VALID' else \
            math.ceil((1 - stride + dilation * (kernel_size - 1)) / 2)


class SqueezeExcitationCNN(MostlyConvCNN):
    @staticmethod
    def _build_conv_blocks(input_channels, c, outchannels):
        blocks = [SEBlock(inchannels=input_channels,
                          outchannels=c.CNN_interm_channels,
                          kernel_size=c.CNN_kernel_size,
                          squeeze_ratio=c.squeeze_ratio)]

        for dilation in c.conv_dilations:
            blocks.append(SEBlock(inchannels=c.CNN_interm_channels,
                                  outchannels=c.CNN_interm_channels,
                                  kernel_size=c.CNN_kernel_size,
                                  squeeze_ratio=c.squeeze_ratio,
                                  dilation=dilation,
                                  padding='SAME'))

        blocks.append(SEBlock(inchannels=c.CNN_interm_channels,
                              outchannels=outchannels,
                              kernel_size=c.CNN_kernel_size,
                              squeeze_ratio=c.squeeze_ratio))
        return nn.Sequential(*blocks)
