import torch
import torch.nn.functional as F

from models.MostlyConvCNN import MostlyConvCNN


class MostlyConvCNNAtt(MostlyConvCNN):
    """
    att_blocks is the same in structure to blocks, with one small change -
    blocks results in a [batch, n_classes * 32, -1] size volume, but att_blocks
    results in a [batch, n_classes, -1] size volume. The channel dimension of att_blocks
    is a learnt mask/attention for each class. We then split blocks into n_classes equal-sized
    partitions along the channel dimension (of size 32), and split the att_blocks into 12
    1-channel partitions. We then pair them up and element-wise multiply and re-concat.
    This has the effect of applying a learnt attention per class to the activations at a late
    stage in the model
    """

    def __init__(self, c, input_dim, input_channels, output_dim):
        super().__init__(c, input_dim, input_channels, output_dim)

        print("With attention!")
        self.att_blocks = self._build_conv_blocks(input_channels, c, output_dim)

    def forward(self, x):
        if x.ndim == 2:
            x = x.unsqueeze(1)
        x = self.avgPool(x)
        x_attention = x

        x = self.blocks(x)

        # Build the attention mask
        x_attention = self.att_blocks(x_attention)
        softmax_dim = 1  # the wrong dimension
        # softmax_dim = -1
        x_attention = F.softmax(x_attention, dim=softmax_dim)

        self._x_attention = x_attention.detach().cpu().numpy()

        # Partition into classes
        num_classes = x_attention.shape[1]
        x_att_split = x_attention.chunk(num_classes, 1)
        x_split = x.chunk(num_classes, 1)

        # Element-wise multiply each class's activation block with the attention vector
        x_out = []
        for x_, att_ in zip(x_split, x_att_split):
            x_out.append(x_ * att_)

        # Re-join before performing the reduction
        x = torch.cat(x_out, 1)

        return self.reduction_block(x)

    def additional_outputs(self):
        return {
            'attention': self._x_attention
        }
