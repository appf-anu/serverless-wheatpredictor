import math

import torch.nn as nn
import torch

from models.BaseModel import BaseModel


class DeepBasicBlock(nn.Module):
    def __init__(self, inchannels, outchannels, kernel_size=3, stride=1, dilation=1, padding='VALID', last=False):
        super().__init__()
        layers = [nn.Conv1d(inchannels,
                            outchannels,
                            kernel_size=kernel_size,
                            stride=stride,
                            padding=self.padding_val(padding, stride, dilation, kernel_size),
                            dilation=dilation)]
        if not last:
            layers.extend([nn.BatchNorm1d(outchannels,
                                          eps=1e-05,
                                          momentum=0.1,
                                          affine=True,
                                          track_running_stats=True),
                           nn.ReLU(inplace=True)])

        self.seq = nn.Sequential(*layers)

    @staticmethod
    def padding_val(policy, stride, dilation, kernel_size):
        """Returns padding size for policies of 'VALID' and 'SAME'"""
        return 0 \
            if policy == 'VALID' else \
            math.ceil((1 - stride + dilation * (kernel_size - 1)) / 2)

    def forward(self, x):
        return self.seq(x)


class ReductionBlock(nn.Module):
    def __init__(self, in_channels, methods, num_classes):
        super().__init__()
        self.in_channels = in_channels
        self.methods = methods
        self.fc = nn.Linear(in_channels * len(methods), num_classes)

    @staticmethod
    def _reduce(x, method):
        if method == 'mean':
            return x.mean(dim=2)
        if method == 'sum':
            return x.sum(dim=2)
        if method == 'prod':
            return x.prod(dim=2)
        if method == 'max':
            return x.max(dim=2)[0]
        if method == 'absmax':
            return ReductionBlock._abs_max(x)
        raise ValueError('Unknown reduce method: {}'.format(method))

    @staticmethod
    def _abs_max(x):
        """Like reducing by max, but using max(abs(x)) instead"""
        batch_size, trait_size = x.shape[:2]
        # For batch-size of 2 with 3 traits:
        # batch_indices = [0, 1]
        # trait_indices = [0, 1, 2]
        batch_indices = torch.arange(0, batch_size)
        trait_indices = torch.arange(0, trait_size)

        # Duplicate values so we can index. For above example:
        # batch_indices = [0, 0, 0, 1, 1, 1]
        # trait_indices = [0, 1, 2, 0, 1, 2]
        [batch_indices, trait_indices] = torch.cartesian_prod(batch_indices, trait_indices).T

        # Index of the max abs value in the last dimension
        abs_max_indices = x.abs().argmax(dim=2).view(-1)

        # Take the values, then shape back to [batch, traits]
        x = x[batch_indices, trait_indices, abs_max_indices]
        return x.view(batch_size, trait_size)

    def forward(self, x):
        reduced_vals = [self._reduce(x, method) for method in self.methods]
        x = torch.cat(reduced_vals, 1)
        return self.fc(x)


class MostlyConvCNN(BaseModel):
    def __init__(self, c, input_dim, input_channels, output_dim):
        super(MostlyConvCNN, self).__init__(c.device)

        second_last_channels = output_dim * 32
        reduce_methods = ['mean', 'absmax', 'max', 'sum']
        # print("Intermediate channels:", c.CNN_interm_channels)
        # print("Channels before reduction:", second_last_channels)
        # print("Reduce methods:", reduce_methods)

        self.avgPool = torch.nn.AvgPool1d(kernel_size=c.CNN_avg_pool_width, stride=c.CNN_avg_pool_width)

        self.blocks = self._build_conv_blocks(input_channels, c, second_last_channels)

        self.reduction_block = ReductionBlock(second_last_channels, reduce_methods, output_dim)

    @staticmethod
    def _build_conv_blocks(input_channels, c, outchannels):
        blocks = [DeepBasicBlock(inchannels=input_channels,
                                 outchannels=c.CNN_interm_channels,
                                 kernel_size=c.CNN_kernel_size)]

        for dilation in c.conv_dilations:
            blocks.append(DeepBasicBlock(inchannels=c.CNN_interm_channels,
                                         outchannels=c.CNN_interm_channels,
                                         kernel_size=c.CNN_kernel_size,
                                         dilation=dilation,
                                         padding='SAME'))

        blocks.append(DeepBasicBlock(inchannels=c.CNN_interm_channels,
                                     outchannels=outchannels,
                                     kernel_size=c.CNN_kernel_size))
        return nn.Sequential(*blocks)

    def forward(self, x):
        if x.ndim == 2:
            x = x.unsqueeze(1)
        x = self.avgPool(x)

        x = self.blocks(x)

        return self.reduction_block(x)
