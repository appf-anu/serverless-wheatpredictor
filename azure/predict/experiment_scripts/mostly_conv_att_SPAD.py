import utils

base_config = 'config/MostlyConvAtt.yml'

# experiments = [
#     {
#         'argv': '',
#         'exp_desc': 'base_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2',
#         'exp_desc': 'base_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2',
#         'exp_desc': 'base_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_SPAD_O_y19'
#     },

#     {
#         'argv': '--dataset v1+v2',
#         'exp_desc': 'base_SPAD_O_v1+v2'
#     },
#     {
#         'argv': '--dataset v1+mex2',
#         'exp_desc': 'base_SPAD_O_v1+mex2'
#     },
#     {
#         'argv': '--dataset v1+y19',
#         'exp_desc': 'base_SPAD_O_v1+y19'
#     },

#     {
#         'argv': '--dataset v1+v2+mex2+y19',
#         'exp_desc': 'base_SPAD_O_v1+v2+mex2+y19'
#     },
# ]
# experiments = [
#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_y19'
#     },
# ]

# experiments = [
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 96',
#         'exp_desc': 'medium'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 96',
#         'exp_desc': 'medium_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 96',
#         'exp_desc': 'medium_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 96',
#         'exp_desc': 'medium_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 64',
#         'exp_desc': 'small'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 64',
#         'exp_desc': 'small_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 64',
#         'exp_desc': 'small_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 64',
#         'exp_desc': 'small_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'smaller'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 16',
#         'exp_desc': 'tiny'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_y19'
#     },
# ]


# experiments = [
#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_v2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_mex2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_y19',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset dwarf19 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_dwarf19',
#         'all_data': True
#     },
#     {
#         'argv': utils.data_filter_str('Aus') + '  --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_Aus',
#         'all_data': True
#     },
#     {
#         'argv': utils.data_filter_str('Mex') + '  --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_Mex',
#         'all_data': True
#     },
#     {
#         'argv': '--CNN_interm_channels 32 --dataset v1+v2+y19+mex2',
#         'exp_desc': 'small_SPAD_O_v1+v2+y19+mex2',
#         'all_data': True
#     },
# ]


# experiments = [
#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_v2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_mex2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_y19',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset dwarf19 --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_dwarf19',
#         'all_data': True
#     },
#     {
#         'argv': utils.data_filter_str('Aus') + '  --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_Aus',
#         'all_data': True
#     },
#     {
#         'argv': utils.data_filter_str('Mex') + '  --CNN_interm_channels 32',
#         'exp_desc': 'small_hiddenwl_SPAD_O_Mex',
#         'all_data': True
#     },
#     {
#         'argv': '--CNN_interm_channels 32 --dataset v1+v2+y19+mex2',
#         'exp_desc': 'small_hiddenwl_SPAD_O_v1+v2+y19+mex2',
#         'all_data': True
#     },
# ]


experiments = [
    {
        'argv': '--CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O',
        'all_data': True
    },
    {
        'argv': '--dataset v2 --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_v2',
        'all_data': True
    },
    {
        'argv': '--dataset mex2 --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_mex2',
        'all_data': True
    },
    {
        'argv': '--dataset y19 --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_y19',
        'all_data': True
    },
    {
        'argv': '--dataset dwarf19 --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_dwarf19',
        'all_data': True
    },
    # {
    #     'argv': utils.data_filter_str('Aus') + '  --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
    #     'exp_desc': 'small_SPAD_O_Aus',
    #     'all_data': True
    # },
    # {
    #     'argv': utils.data_filter_str('Mex') + '  --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
    #     'exp_desc': 'small_SPAD_O_Mex',
    #     'all_data': True
    # },
    {
        'argv': '--CNN_interm_channels 32 --dataset v1+v2+y19+mex2 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_v1+v2+y19+mex2',
        'all_data': True
    },
]
utils.run_experiments(base_config, experiments)
