import utils

base_config = 'config/MostlyConvAtt.yml'

# experiments = [
#     {
#         'argv': '--dataset v2',
#         'exp_desc': 'base_v2',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_oversample',
#     },
#     {
#         'argv': '--dataset v1+v2',
#         'exp_desc': 'base_v1+v2_oversample',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2',
#         'exp_desc': 'base_v1+v2+mex2_oversample',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2+y19',
#         'exp_desc': 'base_v1+v2+mex2+y19_oversample',
#     },
#     {
#         'argv': '--dataset v1+y19',
#         'exp_desc': 'base_v1+y19_oversample',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]"',
#         'exp_desc': 'base_lin_lr_ud__oversample',
#     },
#     {
#         'argv': '--dataset v1+v2 --augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]"',
#         'exp_desc': 'base_v1+v2_lin_lr_ud__oversample',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2 --augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]"',
#         'exp_desc': 'base_v1+v2+mex2_lin_lr_ud__oversample',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2+y19 --augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]"',
#         'exp_desc': 'base_v1+v2+mex2+y19_lin_lr_ud__oversample',
#     }
# ]
# experiments = [
#     {
#         'argv': '--dataset v1+y19',
#         'exp_desc': 'base_v1+y19_oversample'
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_v1+v2_oversample'
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_v1+v2+mex2_oversample'
#     },
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_y19',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2+y19',
#         'exp_desc': 'base_v1+v2+mex2+y19_oversample',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2+y19',
#         'exp_desc': 'base_v1+v2+mex2+y19',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]" --dataset v1+v2',
#         'exp_desc': 'base_v1+v2_base_lin_lr_ud__oversample',
#     },
#     {
#         'argv': '--dataset v1+v2',
#         'exp_desc': 'base_v1+v2_oversample',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_oversample',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base',
#     },
#     {
#         'argv': '--dataset v1+v2+mex2',
#         'exp_desc': 'base_v1+v2+mex2_oversample',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]" --dataset v1+v2+mex2',
#         'exp_desc': 'base_v1+v2+mex2_lin_lr_ud__oversample',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_fix',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_softmax_dim2_fix',
#     },
#     {
#         'argv': '--num_epochs 2000',
#         'exp_desc': 'base_2k'
#     },
#     {
#         'argv': '--num_epochs 5000',
#         'exp_desc': 'base_5k'
#     }
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,5],[\'shift_updown\',0.8,0.01]]"',
#         'exp_desc': 'base_lin_lr_ud',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1],[\'shift_leftright\',0.8,8],[\'shift_updown\',0.8,0.03]]"',
#         'exp_desc': 'base_lin_lr_ud_extreme',
#     },
#     {
#         'argv': '--augmentation "[[\'shift_leftright\',0.8,5]]"',
#         'exp_desc': 'base_leftright-0.8,5',
#     },
#     {
#         'argv': '--augmentation "[[\'shift_leftright\',0.8,10]]"',
#         'exp_desc': 'base_leftright-0.8,10',
#     },
#     {
#         'argv': '--augmentation "[[\'shift_leftright\',0.8,15]]"',
#         'exp_desc': 'base_leftright-0.8,15',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.05]]"',
#         'exp_desc': 'base_linearskew-0.8,0.1',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.1]]"',
#         'exp_desc': 'base_linearskew-0.8,0.1',
#     },
#     {
#         'argv': '--augmentation "[[\'linear_skew\',0.8,0.2]]"',
#         'exp_desc': 'base_linearskew-0.8,0.1',
#     }
# ]

# experiments = [
#     {
#         'argv': '',
#         'exp_desc': 'base_mex2',
#     },
#     {
#         'argv': '',
#         'exp_desc': 'base_y19',
#     },
# ]

# experiments = [
#     {
#         'argv': '',
#         'exp_desc': 'base_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2',
#         'exp_desc': 'base_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2',
#         'exp_desc': 'base_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_SPAD_O_y19'
#     },

#     {
#         'argv': '--dataset v1+v2',
#         'exp_desc': 'base_SPAD_O_v1+v2'
#     },
#     {
#         'argv': '--dataset v1+mex2',
#         'exp_desc': 'base_SPAD_O_v1+mex2'
#     },
#     {
#         'argv': '--dataset v1+y19',
#         'exp_desc': 'base_SPAD_O_v1+y19'
#     },

#     {
#         'argv': '--dataset v1+v2+mex2+y19',
#         'exp_desc': 'base_SPAD_O_v1+v2+mex2+y19'
#     },
# ]
# experiments = [
#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'small_SPAD_O_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 16',
#         'exp_desc': 'smaller_SPAD_O_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 8',
#         'exp_desc': 'tiny_SPAD_O_y19'
#     },
# ]

# experiments = [
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 96',
#         'exp_desc': 'medium'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 96',
#         'exp_desc': 'medium_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 96',
#         'exp_desc': 'medium_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 96',
#         'exp_desc': 'medium_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 64',
#         'exp_desc': 'small'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 64',
#         'exp_desc': 'small_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 64',
#         'exp_desc': 'small_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 64',
#         'exp_desc': 'small_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 32',
#         'exp_desc': 'smaller'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 32',
#         'exp_desc': 'smaller_y19'
#     },

#     {
#         'argv': '--CNN_interm_channels 16',
#         'exp_desc': 'tiny'
#     },
#     {
#         'argv': '--dataset v2 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_v2'
#     },
#     {
#         'argv': '--dataset mex2 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_mex2'
#     },
#     {
#         'argv': '--dataset y19 --CNN_interm_channels 16',
#         'exp_desc': 'tiny_y19'
#     },
# ]


experiments = [
    {
        'argv': utils.data_filter_str('Aus'),
        'exp_desc': 'base_Aus',
        'mex_aus_split': True
    },
    {
        'argv': utils.data_filter_str('Mex'),
        'exp_desc': 'base_Mex',
        'mex_aus_split': True
    },
]

utils.run_experiments(base_config, experiments)
