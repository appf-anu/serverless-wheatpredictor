import utils

base_config = 'config/ThiMultiTask.yml'

# experiments = [
#     {
#         'argv': '',
#         'exp_desc': 'base',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset v2',
#         'exp_desc': 'base_v2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset mex2',
#         'exp_desc': 'base_mex2',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset y19',
#         'exp_desc': 'base_y19',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset dwarf19',
#         'exp_desc': 'base_dwarf19',
#         'all_data': True
#     },
#     {
#         'argv': '--dataset v1+v2+y19+mex2',
#         'exp_desc': 'base_v1+v2+y19+mex2',
#         'all_data': True
#     },
# ]
experiments = [
    # {
    #   'argv':  '--dataset v1+v2+y19+mex2 --variable_length',
    #   'exp_desc': 'base_vwl_v1+v2+y19+mex2',
    #   'all_data': True
    # },
    {
        'argv': '--dataset v1',
        'exp_desc': 'base_v1',
        # 'all_data': True
    },
    # {
    #   'argv':  '--dataset v1+v2+y19+mex2 --variable_length --include_wavelengths',
    #   'exp_desc': 'base_vwl-wl_v1+v2+y19+mex2',
    #   'all_data': True
    # },
    # {
    #   'argv':  '--dataset v1+v2+y19+mex2 --include_wavelengths',
    #   'exp_desc': 'base_wl_v1+v2+y19+mex2',
    #   'all_data': True
    # },
]

experiments = [
    {
        'argv': '--dataset v1 --CNN_num_channels 256',
        'exp_desc': 'base_256c_v1',
    },
    {
        'argv': '--dataset v1 --CNN_num_channels 128',
        'exp_desc': 'base_128c_v1',
    },
    {
        'argv': '--dataset v1 --CNN_num_channels 64',
        'exp_desc': 'base_64c_v1',
    },
    {
        'argv': '--dataset v1 --CNN_num_channels 32',
        'exp_desc': 'base_32c_v1',
    },

    {
        'argv': '--dataset v1 --variable_length --CNN_num_channels 256',
        'exp_desc': 'base_vl_256c_v1',
    },
    {
        'argv': '--dataset v1 --variable_length --CNN_num_channels 128',
        'exp_desc': 'base_vl_128c_v1',
    },
    {
        'argv': '--dataset v1 --variable_length --CNN_num_channels 64',
        'exp_desc': 'base_vl_64c_v1',
    },
    {
        'argv': '--dataset v1 --variable_length --CNN_num_channels 32',
        'exp_desc': 'base_vl_32c_v1',
    },
]
experiments = [
    # {
    #     'argv': '--dataset v1 --CNN_num_channels 300',
    #     'exp_desc': 'base_300c_v1',
    # },
    # {
    #     'argv': '--dataset v1 --variable_length --CNN_num_channels 300',
    #     'exp_desc': 'base_vl_300c_v1',
    # },
    {
        'argv': '--dataset v1+v2+y19+mex2 --variable_length --CNN_num_channels 300',
        'exp_desc': 'base_vl_300c_v1+v2+y19+mex2',
    },
]


utils.run_experiments(base_config, experiments, num_parallel=2)
