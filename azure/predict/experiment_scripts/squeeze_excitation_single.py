import utils
import dataset.constants as constants


base_config = 'config/SqueezeExcitationCNNSingleTrait.yml'


experiments = []
squeeze_ratio = 8
# for trait in constants.TRAITS:
for trait in ['SPAD_O', 'LMA_O']:
    experiments.append({
        'argv': f'--dataset v1+v2+y19+mex2 {utils.traits_string(trait)}',
        'exp_desc': f'base_{trait}_v1+v2+y19+mex2',
        'all_data': True
    })

experiments = []
squeeze_ratio = 8
# for trait in constants.TRAITS:
for trait in constants.TRAITS:
    experiments.append({
        'argv': f'--dataset v1 {utils.traits_string(trait)}',
        'exp_desc': f'base_{trait}_v1',
        # 'all_data': True
    })
utils.run_experiments(base_config, experiments, num_parallel=2)
