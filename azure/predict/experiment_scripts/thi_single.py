import utils
from dataset import constants

base_config = 'config/ThiSingleTask.yml'

experiments = []

for trait in ['SPAD_O', 'LMA_O']:
    experiments.append({
        'argv': f'--dataset v1+v2+y19+mex2 {utils.traits_string(trait)}',
        'exp_desc': f'base_{trait}_v1+v2+y19+mex2',
        'all_data': True
    })

hidden_size = 50

experiments = []
for trait in constants.TRAITS:
    experiments.append({
        'argv': f'--dataset v1 {utils.traits_string(trait)} --variable_length --CNN_num_channels {hidden_size}',
        'exp_desc': f'base_vl_{hidden_size}c_{trait}_v1',
        # 'all_data': True
    })


experiments = []
for trait in constants.TRAITS:
    experiments.append({
        'argv': f'--dataset v1+v2+y19+mex2 {utils.traits_string(trait)} --variable_length --CNN_num_channels {hidden_size}',
        'exp_desc': f'base_vl_{hidden_size}c_{trait}_v1+v2+y19+mex2',
        # 'all_data': True
    })

utils.run_experiments(base_config, experiments, num_parallel=2)
