import utils

base_config = 'config/MostlyConv.yml'

experiments = [
    {
        'argv': '--dataset mex2 --CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
        'exp_desc': 'small_SPAD_O_mex2',
        'all_data': True
    },
    {
        'argv': '--dataset mex2 --CNN_interm_channels 32 ' + utils.traits_string('LMA_O'),
        'exp_desc': 'small_LMA_O_mex2',
        'all_data': True
    },
    # {
    #   'argv'    : '--CNN_interm_channels 32 ' + utils.traits_string('SPAD_O'),
    #   'exp_desc': 'small_SPAD_O',
    #   'all_data': True
    # },
]
# python3 visualise_model.py --config_file config/MostlyConvAtt.yml --exp_desc MostlyConvAtt/small_SPAD_O --CNN_interm_channels 32 --traits [\"SPAD_O\"]
"""
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_SPAD_O --CNN_interm_channels 32 --traits [\"SPAD_O\"]
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_LMA_O --CNN_interm_channels 32 --traits [\"LMA_O\"]

python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_SPAD_O_v2 --CNN_interm_channels 32 --traits [\"SPAD_O\"] --dataset v2
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_LMA_O_v2 --CNN_interm_channels 32 --traits [\"LMA_O\"] --dataset v2

python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_SPAD_O_mex2 --CNN_interm_channels 32 --traits [\"SPAD_O\"] --dataset mex2
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_LMA_O_mex2 --CNN_interm_channels 32 --traits [\"LMA_O\"] --dataset mex2

python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_SPAD_O_y19 --CNN_interm_channels 32 --traits [\"SPAD_O\"] --dataset y19
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_LMA_O_y19 --CNN_interm_channels 32 --traits [\"LMA_O\"] --dataset y19

python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_SPAD_O_dwarf19 --CNN_interm_channels 32 --traits [\"SPAD_O\"] --dataset dwarf19
python3 visualise_model.py --config_file config/MostlyConv.yml --exp_desc MostlyConv/small_LMA_O_dwarf19 --CNN_interm_channels 32 --traits [\"LMA_O\"] --dataset dwarf19
"""
utils.run_experiments(base_config, experiments)
