"""Process the v2 csv data file and store it in `/data/processed`"""
import os
import pandas as pd

import constants


DATASET_VERSION = 'v2'

in_path = os.path.join(constants.RAW_DIR, DATASET_VERSION, "Mtrx-Valexp-Arch18-forZhen.csv")
out_path = os.path.join(constants.DATAFRAME_DIR, f'dataset_{DATASET_VERSION}.csv')

new_to_old_name = {
    'n_area': 'Narea_O',
    'Photo': 'Photo_O',
    'Cond': 'Cond_O',
    'lma': 'LMA_O',
    'n_mass': 'Nmass_O',
    'SPAD': 'SPAD_O'
}


def read_file(path):
    df = pd.read_csv(path)
    df = constants.apply_df_filter(df)

    return df


df = read_file(in_path)
df = df.rename(columns=new_to_old_name)

# Add the derived trait
df['Vcmax25_Narea_O'] = df['Vcmax25'] / df['Narea_O']

print(df)
print("Saving...")
df.to_csv(out_path, index=False, header=True)
print("Written to", out_path)
