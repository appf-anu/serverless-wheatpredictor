import numpy as np
import pandas as pd

# 0.7 train, 0.1 validation and 0.2 test
SPLIT_RATIOS = [.7, .1, .2]


def get_train_val_test_split(data_file, traits, data_set_num):
    df = pd.read_csv(data_file)
    df['col_index'] = range(0, len(df))
    filtered_general = df[df["Wave_350"].notnull()].query('Wave_800>0.35').query('Wave_800<0.6')
    if (data_set_num == 1):
        filtered_general = filtered_general[filtered_general['Exp'] != 'CB_Mex']
    # filter nan
    for trait in traits:
        filtered_general = filtered_general[filtered_general[trait].notnull()]

    train = filtered_general.sample(frac=SPLIT_RATIOS[0])
    val = filtered_general.drop(train.index).sample(frac=(SPLIT_RATIOS[1] / (1 - SPLIT_RATIOS[0])))
    test = filtered_general.drop(train.index).drop(val.index)

    return train['col_index'].values, val['col_index'].values, test['col_index'].values


# Traits: LMA, Narea, SPAD, Nmass, Parea, Pmass
DataFile0 = "/data/Mtrx-LMA-Narea-SPAD_forZH.csv"

# Traits: Vcmax, Vcmax25, J, A (Photo_O), gs (Cond_O)
DataFile1 = "/data/Out_Mtrx_Vcmax-Vc25-J_forZH.csv"

Traits0 = ["LMA_O", "Narea_O", "SPAD_O", "Nmass_O", "Parea_O", "Pmass_O"]
Traits1 = ["Vcmax", "Vcmax25", "J", "Photo_O", "Cond_O"]

for data_set_num in range(0, 2):
    np.random.seed(42)
    if (data_set_num == 0):
        current_trait_set = Traits0
        current_file = DataFile0
        traitset = "Traits0"
    else:
        current_trait_set = Traits1
        current_file = DataFile1
        traitset = "Traits1"

    print("processing: ", current_file, current_trait_set, data_set_num)
    train, val, test = get_train_val_test_split(current_file, current_trait_set, data_set_num)
    print("train size: " + str(len(train)) + " val size:" + str(len(val)) + " test size:" + str(len(test)))

    np.savez('/data/splits/split_' + traitset + '.npz', train=train, val=val, test=test)
