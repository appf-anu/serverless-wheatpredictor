import numpy as np


def shift_updown(values, rate_percent=0.0, rate=0.0):
    values = (values + values * rate_percent) + rate
    return values


def shift_leftright(values, step=0):
    values = np.roll(values, step)
    return values


def mix_up(X1, Y1, X2, Y2, alpha):
    new_X = (1.0 - alpha) * X1 + alpha * X2
    new_Y = (1.0 - alpha) * Y1 + alpha * Y2

    return new_X, new_Y
