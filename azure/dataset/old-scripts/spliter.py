import os
import numpy as np
from functools import reduce
import pandas as pd

# 0.7 train, 0.1 validation and 0.2 test
SPLIT_RATIOS = [.7, .1, .2]

data_dir = "/data"


def get_train_val_test_split(data_file, trait, data_set_num):
    df = pd.read_csv(data_file)
    df['col_index'] = range(0, len(df))

    if trait not in df.columns:
        print(trait, "not in dataset")
        return

    if 'Wave_350' in df.columns:
        df = df[df["Wave_350"].notnull()]

    filtered_general = df.query('Wave_800>0.35').query('Wave_800<0.6')
    if (data_set_num == 1):
        filtered_general = filtered_general[filtered_general['Exp'] != 'CB_Mex']
    filtered_trait_NA = filtered_general[filtered_general[trait].notnull()]
    train = filtered_trait_NA.sample(frac=SPLIT_RATIOS[0])
    val = filtered_trait_NA.drop(train.index).sample(frac=(SPLIT_RATIOS[1] / (1 - SPLIT_RATIOS[0])))
    test = filtered_trait_NA.drop(train.index).drop(val.index)

    return train['col_index'].values, val['col_index'].values, test['col_index'].values


# Traits: LMA, Narea, SPAD, Nmass, Parea, Pmass
DataFile0 = data_dir + "/Mtrx-LMA-Narea-SPAD_forZH.csv"

# Traits: Vcmax, Vcmax25, J, A (Photo_O), gs (Cond_O)
DataFile1 = data_dir + "/Out_Mtrx_Vcmax-Vc25-J_forZH.csv"

v2_datafile = os.path.join(data_dir, 'merged', 'dataset_2.csv')

Traits0 = ["LMA_O", "Narea_O", "SPAD_O", "Nmass_O", "Parea_O", "Pmass_O"]
Traits1 = ["Vcmax", "Vcmax25", "J", "Photo_O", "Cond_O"]

joint_indices = {
    'names': ["Vcmax25", "Narea_O"],
    'train': [],
    'val': [],
    'test': []
}

dataset = 'v1'


def save(data_dir, trait, train, val, test):
    print("Trait:", trait)
    print("\ttrain size: " + str(len(train)) + " val size:" + str(len(val)) + " test size:" + str(len(test)))
    if dataset == 'v2':
        trait = trait + '_v2'
    np.savez(data_dir + '/splits/split_' + trait + '.npz', train=train, val=val, test=test)


if dataset == 'v2':
    for trait in Traits0 + Traits1 + ['Vcmax25_Narea_O']:
        print("processing: ", v2_datafile, trait)
        out = get_train_val_test_split(v2_datafile, trait, data_set_num=0)
        if out is not None:
            train, val, test = out
            save(data_dir, trait, train, val, test)
else:
    for data_set_num in range(0, 2):
        if (data_set_num == 0):
            current_trait_set = Traits0
            current_file = DataFile0
        else:
            current_trait_set = Traits1
            current_file = DataFile1

        for trait in current_trait_set:
            print("processing: ", current_file, trait, data_set_num)
            np.random.seed(42)
            train, val, test = get_train_val_test_split(current_file, trait, data_set_num)
            if trait in joint_indices['names']:
                joint_indices['train'].append(train)
                joint_indices['val'].append(val)
                joint_indices['test'].append(test)
            print(trait, ":", len(train))
            save(data_dir, trait, train, val, test)

    j_train, j_val, j_test = None, None, None
    j_name = '_'.join(joint_indices['names'])
    joint_indices['train'] = reduce(np.intersect1d, joint_indices['train'])
    joint_indices['val'] = reduce(np.intersect1d, joint_indices['val'])
    joint_indices['test'] = reduce(np.intersect1d, joint_indices['test'])
    save(data_dir, j_name, joint_indices['train'], joint_indices['val'], joint_indices['test'])
