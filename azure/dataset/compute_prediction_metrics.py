"""
Compute the R^2 value and correlation between the ground-truth and predictions in
an unprocessed dataset, as received from ANU.
"""
import os
import sys
import numpy as np
import pandas as pd
from sklearn.metrics import r2_score as r2_score
from scipy.stats import pearsonr

import constants


datasets = {
    'y19': {
        'version': 'y19',
        'file': 'y19.asd.final.csv',
        'gt_col': 'SPAD',
        'pred_col': 'SPADpred'
    },
    'dwarf19_SPAD': {
        'version': 'dwarf19',
        'file': 'dfrDwarf2019.csv',
        'gt_col': 'SPAD',
        'pred_col': 'SPAD_Pred'
    },
    'dwarf19_LMA': {
        'version': 'dwarf19',
        'file': 'dfrDwarf2019.csv',
        'gt_col': 'LMA_g_m2',
        'pred_col': 'LMA_O'
    }
}

DATASET = datasets['dwarf19_LMA']

in_path = os.path.join(constants.RAW_DIR, DATASET['version'], DATASET['file'])

df = pd.read_csv(in_path)
df = constants.apply_df_filter(df)

groundtruth = df[DATASET['gt_col']]
predictions = df[DATASET['pred_col']]

print("R^2: {:.5f}".format(r2_score(groundtruth, predictions)))
print("Pearson correlation: {:.5f}".format(pearsonr(groundtruth, predictions)[0]))
print("GT Mean; STD: {:.5f}; {:.5f}".format(np.mean(groundtruth), np.std(groundtruth)))
print("PR Mean; STD: {:.5f}; {:.5f}".format(np.mean(predictions), np.std(predictions)))

if '--vis' in sys.argv:
    import matplotlib.pyplot as plt
    minv = min(np.percentile(groundtruth, 1), np.percentile(predictions, 1))
    maxv = max(np.percentile(groundtruth, 99), np.percentile(predictions, 99))

    identity = np.linspace(minv, maxv, 2)
    plt.plot(identity, identity, 'black', alpha=0.25)

    plt.xlim(minv, maxv)
    plt.ylim(minv, maxv)
    plt.plot(groundtruth, predictions, 'b.')

    plt.show()
