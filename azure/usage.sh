#!/bin/bash

export $(cat .env | xargs)
RESOURCE_GROUP_NAME=${APP_NAME}-rg

az monitor metrics list \
    --resource /subscriptions/${SUBSCRIPTION_ID}/resourceGroups/${RESOURCE_GROUP_NAME}/providers/Microsoft.Web/sites/${APP_NAME} \
    --metric FunctionExecutionUnits,FunctionExecutionCount \
    --aggregation Total \
    --interval PT1H --start-time $(date -Is --date '-7day') --end-time $(date -Is) |jq
